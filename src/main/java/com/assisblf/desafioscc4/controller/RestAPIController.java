package com.assisblf.desafioscc4.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin
@RestController
@Api(description = "REST API endpoints do desafio SCC4.")
public class RestAPIController {

	@GetMapping("listaReversa")
	@ApiOperation("Dado uma lista de números, imprimir a lista de trás pra frente.")
	public List<Integer> listaReversa(@RequestParam List<Integer> numeros) {
		List<Integer> listaReversa = new ArrayList<Integer>(numeros);
		Collections.reverse(listaReversa);
		return listaReversa;
	}

	@GetMapping("imprimirImpares")
	@ApiOperation("Dado uma lista de números, imprimir todos os numeros impares.")
	public List<Integer> imprimirImpares(@RequestParam List<Integer> numeros) {
		return numeros.stream().filter(i -> i%2 != 0).collect(Collectors.toList());
	}

	@GetMapping("imprimirPares")
	@ApiOperation("Dado uma lista de números, imprimir todos os numeros pares.")
	public List<Integer> imprimirPares(@RequestParam List<Integer> numeros) {
		return numeros.stream().filter(i -> i%2 == 0).collect(Collectors.toList());
	}

	@GetMapping("imprimirTamanhoPalavra")
	@ApiOperation("Dado uma palavra, conta o tamanho dela.")
	public Integer imprimirTamanhoPalavra(@RequestParam String palavra) {
		return palavra.length();
	}

	@GetMapping("imprimirLetrasMaiusculas")
	@ApiOperation("Dado uma lista de palavras, devolve todas em letras maiúsculas.")
	public List<String> imprimirLetrasMaiusculas(@RequestParam List<String> palavras) {
		return palavras.stream().map(i -> i.toUpperCase()).collect(Collectors.toList());
	}

	@GetMapping("imprimirLetrasMinusculas")
	@ApiOperation("Dado uma lista de palavras, devolve todas em letras minúsculas.")
	public List<String> imprimirLetrasMinusculas(@RequestParam List<String> palavras) {
		return palavras.stream().map(i -> i.toLowerCase()).collect(Collectors.toList());
	}

	@GetMapping("imprimirVogais")
	@ApiOperation("Dado uma lista de palavras, devolve todas as vogais.")
	public String imprimirVogais(@RequestParam List<String> palavras) {
		StringBuilder stringBuilder = new StringBuilder();
		for (String palavra : palavras) {
			String vogais = palavra.replaceAll("[^aeiou]", "");
			stringBuilder.append(vogais);
		}
		return stringBuilder.toString();
	}

	@GetMapping("imprimirConsoantes")
	@ApiOperation("Dado uma lista de palavras, devolve todas as consoantes.")
	public String imprimirConsoantes(@RequestParam List<String> palavras) {
		StringBuilder stringBuilder = new StringBuilder();
		for (String palavra : palavras) {
			String consoantes = palavra.replaceAll("[^a-zZ-Z]", "").replaceAll("[aeiou]", "");
			stringBuilder.append(consoantes);
		}
		return stringBuilder.toString();
	}

	@GetMapping("imprimirContagemDeVogais")
	@ApiOperation("Dado uma lista de palavras, conta todas as vogais.")
	public Integer imprimirContagemDeVogais(@RequestParam List<String> palavras) {
		return imprimirVogais(palavras).length();
	}

	@GetMapping("imprimirContagemDeConsoantes")
	@ApiOperation("Dado uma lista de palavras, conta todas as consoantes.")
	public Integer imprimirContagemDeConsoantes(@RequestParam List<String> palavras) {
		return imprimirConsoantes(palavras).length();
	}

	@GetMapping("imprimirArrayDePalavras")
	@ApiOperation("Dado uma frase, devolve um array de palavras.")
	public List<String> imprimirArrayDePalavras(String frase) {
		return Arrays.asList(frase.split(" "));
	}

	@GetMapping("imprimirNomeBibliografico")
	@ApiOperation("Dado um nome, transforma-o em nome bibliográfico, levando em conta que o último nome é o sobrenome absoluto.")
	public String imprimirNomeBibliografico(String nomeCompleto) {
		LinkedList<String> palavras = new LinkedList<String>(imprimirArrayDePalavras(nomeCompleto));
		if (palavras.size() < 2) {
			return nomeCompleto;
		}
		String ultimoSobrenomeEstimado = palavras.removeLast().toUpperCase();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(ultimoSobrenomeEstimado);
		stringBuilder.append(",");
		for (String palavra : palavras) {
			stringBuilder.append(" ").append(palavra);
		}
		return stringBuilder.toString();
	}

	@GetMapping("menorQuantidadeDeNotas")
	@ApiOperation("Dado um valor, retorna a menor quantidade de notas de R$3 e R$5 para completá-lo. Se não for possível, exibe a mensagem \"Não foi possível realizar o saque.\".")
	public String menorQuantidadeDeNotas(Integer valor) {
		int qtde5 = valor/5;
		int sobra = valor - qtde5*5;
		while (sobra % 3 != 0) {
			qtde5--;
			sobra += 5;
		}
		if (qtde5 < 0) {
			return "Não foi possível realizar o saque.";
		}
		int qtde3 = sobra/3;
		sobra = sobra - qtde3*3;
		return contagemDeNotas(qtde5, qtde3);
	}

	private String contagemDeNotas(int qtde5, int qtde3) {
		StringBuilder stringBuilder = new StringBuilder();
		if (qtde3 != 0) {
			stringBuilder.append(qtde3).append("x3");
		}
		if (qtde5 != 0) {
			if (qtde3 != 0) {
				stringBuilder.append(" e ");
			}
			stringBuilder.append(qtde5).append("x5");
		}
		return stringBuilder.toString();
	}

}
