package com.assisblf.desafioscc4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioScc4Application {

	public static void main(String[] args) {
		SpringApplication.run(DesafioScc4Application.class, args);
	}

}
