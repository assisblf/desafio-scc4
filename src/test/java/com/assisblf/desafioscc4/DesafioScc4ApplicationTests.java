package com.assisblf.desafioscc4;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.assisblf.desafioscc4.controller.RestAPIController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DesafioScc4ApplicationTests {

	@Autowired
	private RestAPIController restAPIController;
	
	private List<Integer> integerArrayInput = Arrays.asList(25,35,40,21);
	private List<String> upperCaseArrayInput = Arrays.asList("TESTE", "BATATA");
	private List<String> lowerCaseArrayInput = Arrays.asList("teste", "batata");

	@Test
	public void listaReversaRecupera() {
		List<Integer> input = integerArrayInput;
		List<Integer> expected = Arrays.asList(21,40,35,25);
		List<Integer> real = restAPIController.listaReversa(input);
		Assert.assertEquals(expected, real);
	}
	
	@Test
	public void imprimirImpares() {
		List<Integer> input = integerArrayInput;
		List<Integer> expected = Arrays.asList(25,35,21);
		List<Integer> real = restAPIController.imprimirImpares(input);
		Assert.assertEquals(expected, real);
	}
	
	@Test
	public void imprimirPares() {
		List<Integer> input = integerArrayInput;
		List<Integer> expected = Arrays.asList(40);
		List<Integer> real = restAPIController.imprimirPares(input);
		Assert.assertEquals(expected, real);
	}
	
	@Test
	public void imprimirTamanhoPalavra() {
		String input = "sapinho";
		Integer expected = 7;
		Integer real = restAPIController.imprimirTamanhoPalavra(input);
		Assert.assertEquals(expected, real);
	}
	
	@Test
	public void imprimirLetrasMaiusculas() {
		List<String> input = lowerCaseArrayInput;
		List<String> expected = upperCaseArrayInput;
		List<String> real = restAPIController.imprimirLetrasMaiusculas(input);
		Assert.assertEquals(expected, real);
	}
	
	@Test
	public void imprimirLetrasMinusculas() {
		List<String> input = upperCaseArrayInput;
		List<String> expected = lowerCaseArrayInput;
		List<String> real = restAPIController.imprimirLetrasMinusculas(input);
		Assert.assertEquals(expected, real);
	}
	
	@Test
	public void imprimirVogais() {
		List<String> input = lowerCaseArrayInput;
		String expected = "eeaaa";
		String real = restAPIController.imprimirVogais(input);
		Assert.assertEquals(expected, real);
	}
	
	@Test
	public void imprimirConsoantes() {
		List<String> input = lowerCaseArrayInput;
		String expected = "tstbtt";
		String real = restAPIController.imprimirConsoantes(input);
		Assert.assertEquals(expected, real);
	}
	
	@Test
	public void imprimirContagemDeVogais() {
		List<String> input = lowerCaseArrayInput;
		Integer expected = 5;
		Integer real = restAPIController.imprimirContagemDeVogais(input);
		Assert.assertEquals(expected, real);
	}
	
	@Test
	public void imprimirContagemDeConsoantes() {
		List<String> input = lowerCaseArrayInput;
		Integer expected = 6;
		Integer real = restAPIController.imprimirContagemDeConsoantes(input);
		Assert.assertEquals(expected, real);
	}
	
	@Test
	public void imprimirArrayDePalavras() {
		String input = "Pedro é chavequeiro";
		List<String> expected = Arrays.asList("Pedro","é","chavequeiro");
		List<String> real = restAPIController.imprimirArrayDePalavras(input);
		Assert.assertEquals(expected, real);
	}
	
	@Test
	public void imprimirNomeBibliografico() {
		String input = "Brunno Almeida";
		String expected = "ALMEIDA, Brunno";
		String real = restAPIController.imprimirNomeBibliografico(input);
		Assert.assertEquals(expected, real);
	}

	@Test
	public void saque6() {
		saque(6, "2x3");
	}

	@Test
	public void saque7() {
		saque(7, "Não foi possível realizar o saque.");
	}

	@Test
	public void saque8() {
		saque(8, "1x3 e 1x5");
	}

	@Test
	public void saque11() {
		saque(11, "2x3 e 1x5");
	}

	@Test
	public void saque13() {
		saque(13, "1x3 e 2x5");
	}

	@Test
	public void saque30() {
		saque(30, "6x5");
	}

	@Test
	public void saque50() {
		saque(50, "10x5");
	}

	@Test
	public void saque56() {
		saque(56, "2x3 e 10x5");
	}
	
	public void saque(int valorSacado, String resultadoEsperado) {
		Integer input = valorSacado;
		String real = restAPIController.menorQuantidadeDeNotas(input);
		Assert.assertEquals(resultadoEsperado, real);
	}
	
}
