### Pré-requisitos para a instalação
- Java 1.8
- git
- maven
- porta 8080 disponível para uso

### Passos
1. Clonar o projeto para o seu ambiente
2. Acessar o diretório do projeto via linha de comando
3. Executar o comando *mvn spring-boot:run*
4. Abrir a página http://localhost:8080 do seu navegador para ver os API endpoints disponíveis